// Modified by @author Tapani Jamsa

/*
 * Geopaparazzi - Digital field mapping on Android based devices
 * Copyright (C) 2010  HydroloGIS (www.hydrologis.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package eu.geopaparazzi.library.kml;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.os.Environment;
import eu.geopaparazzi.library.util.CompressionUtilities;
import eu.geopaparazzi.library.util.ResourcesManager;
import eu.geopaparazzi.library.util.debug.Debug;
import eu.geopaparazzi.library.util.debug.Logger;
import eu.hydrologis.geopaparazzi.util.Image;

/**
 * A kmz exporter for notes, logs and pics.
 * 
 * @author Andrea Antonello (www.hydrologis.com)
 */
@SuppressWarnings("nls")
public class KmzExport {

	// private final File outputFile;
	private String name;
	private float leftFov = -26.6666666667f;
	private float rightFov = 26.6666666667f;
	private float bottomFov = -20f;
	private float topFov = 20f;
	private float near = 10f;

	/**
	 * Constructor.
	 * 
	 * @param name
	 *            a name for the kmz.
	 * @param outputFile
	 *            the path in which to create the kmz.
	 */
	public KmzExport(String name) {
		this.name = name;
		// this.outputFile = outputFile;
	}

	public void export(Context context, Image image) throws IOException {
		if (name == null) {
			name = "Wanderoid Export";
		}

		File applicationDir = ResourcesManager.getInstance(context).getApplicationDir();

		List<File> existingImages = new ArrayList<File>();

		/*
		 * write the internal kml file
		 */
		String kmlExportDir = Environment.getExternalStorageDirectory() + File.separator + "Wanderoid" + File.separator + "kml";
		String filename = "kml.kml";
		File kmlFolder = new File(kmlExportDir);
		File kmlFile = new File(kmlFolder, filename);
		BufferedWriter bW = null;
		try {
			bW = new BufferedWriter(new FileWriter(kmlFile));
			bW.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
			bW.write("<kml xmlns=\"http://www.opengis.net/kml/2.2\" xmlns:gx=\"http://www.google.com/kml/ext/2.2\"\n");
			bW.write("xmlns:kml=\"http://www.opengis.net/kml/2.2\" xmlns:atom=\"http://www.w3.org/2005/Atom\">\n");
			bW.write("<Document>\n");
			if (image.hasImages()) {
				bW.write("<PhotoOverlay id=\"snapshot\">\n");
			} else {
				bW.write("<PhotoOverlay id=\"sensordata\">\n");
			}
			bW.write("<name>\n");
			if (image.hasImages()) {
				bW.write("snapshot.jpg\n");
			} else {
				bW.write("sensordata\n");
			}
			bW.write("</name>\n");
			bW.write("<Camera>\n");
			bW.write("<gx:TimeStamp><when>" + image.getTs() + "</when>\n");
			bW.write("</gx:TimeStamp>\n");
			bW.write("<longitude>" + image.getLon() + "</longitude>\n");
			bW.write("<latitude>" + image.getLat() + "</latitude>\n");
			bW.write("<heading>" + image.getAzim() + "</heading>\n");
			bW.write("<altitude>" + image.getAltim() + "</altitude>\n");
			if (Debug.D)
				Logger.d(this, "<longitude>" + image.getLon() + "</longitude>\n");
			if (Debug.D)
				Logger.d(this, "<heading>" + image.getAzim() + "</heading>\n");
			if (Debug.D)
				Logger.d(this, "image.getAccelZ()" + image.getAccelZ() + "\n");
			if (image.getAccelZ() >= 0) {
				double pitch = image.getPitch();
				double tilt = Math.abs(pitch);
				bW.write("<tilt>" + tilt + "</tilt>\n");

				double roll = image.getRoll() * -1;
				bW.write("<roll>" + roll + "</roll>\n");

				if (Debug.D)
					Logger.d(this, "roll1 " + roll);
				if (Debug.D)
					Logger.d(this, "tilt1 " + tilt);
			} else {
				double pitch = image.getPitch();
				double tilt = 90 + (100 - Math.abs(pitch));
				bW.write("<tilt>" + tilt + "</tilt>\n");

				double roll = 0;
				if (image.getRoll() >= 0) {
					roll = Math.abs(image.getPitch()) - 90;
				} else {
					roll = (Math.abs(image.getPitch()) - 90) * -1;
				}
				bW.write("<roll>" + roll + "</roll>\n");

				if (Debug.D)
					Logger.d(this, "roll2 " + roll);
				if (Debug.D)
					Logger.d(this, "tilt2 " + tilt);
			}
			bW.write("<altitudeMode>absolute</altitudeMode>\n");
			bW.write("</Camera>\n");
			bW.write("<Icon>\n");
			bW.write("<href>\n");
			if (image.hasImages()) {
				bW.write("snapshot.jpg\n");
			}
			else{
				bW.write("sensordata\n");
			}
			bW.write("</href>\n");
			bW.write("</Icon>\n");
			bW.write("<ViewVolume>\n");
			bW.write("<leftFov>\n");
			bW.write(leftFov + "\n");
			bW.write("</leftFov>\n");
			bW.write("<rightFov>\n");
			bW.write(rightFov + "\n");
			bW.write("</rightFov>\n");
			bW.write("<bottomFov>\n");
			bW.write(bottomFov + "\n");
			bW.write("</bottomFov>\n");
			bW.write("<topFov>\n");
			bW.write(topFov + "\n");
			bW.write("</topFov>\n");
			bW.write("<near>\n");
			bW.write(near + "\n");
			bW.write("</near>\n");
			bW.write("</ViewVolume>\n");
			bW.write("<Point>\n");
			bW.write("<coordinates>\n");
			bW.write(image.getLat() + ", " + image.getLon() + "\n");
			bW.write("</coordinates>\n");
			bW.write("</Point>\n");
			bW.write("</PhotoOverlay>\n");
			bW.write("</Document>\n");
			bW.write("</kml>\n");

			// addMarker(bW, "red-pushpin",
			// "http://maps.google.com/mapfiles/kml/pushpin/red-pushpin.png",
			// 20, 2);
			// addMarker(bW, "yellow-pushpin",
			// "http://maps.google.com/mapfiles/kml/pushpin/ylw-pushpin.png",
			// 20, 2);
			// addMarker(bW, "bookmark-icon",
			// "http://maps.google.com/mapfiles/kml/pal4/icon39.png", 16, 16);
			// addMarker(bW, "camera-icon",
			// "http://maps.google.com/mapfiles/kml/pal4/icon38.png", 16, 16);
			// addMarker(bW, "info-icon",
			// "http://maps.google.com/mapfiles/kml/pal3/icon35.png", 16, 16);

			// for( KmlRepresenter kmlRepresenter : image ) {
			// try {
			// bW.write(image.toKmlString());

			// if (image.hasImages()) {
			// List<String> imagePaths = kmlRepresenter.getImagePaths();
			// for( String imagePath : imagePaths ) {
			// File imageFile = new File(applicationDir, imagePath);
			// if (imageFile.exists()) {
			// existingImages.add(imageFile);
			// } else {
			// Logger.w(this, "Can't find image: " +
			// imageFile.getAbsolutePath());
			// }
			// }
			// }
			// } catch (Exception e) {
			// Logger.e(this, e.getLocalizedMessage(), e);
			// e.printStackTrace();
			// }
			// }

			// bW.write("</Document>\n");
			// bW.write("</kml>\n");

		} finally {
			if (bW != null)
				bW.close();
		}
		// /*
		// * create the kmz file with the base kml and all the pictures
		// */
		//
		// File[] files = new File[1 + existingImages.size()];
		// files[0] = kmlFile;
		// for( int i = 0; i < files.length - 1; i++ ) {
		// files[i + 1] = existingImages.get(i);
		// }
		// CompressionUtilities.createZipFromFiles(outputFile, files);
		//
		// kmlFile.delete();
	}

	private void addMarker(BufferedWriter bW, String alias, String url, int x, int y) throws IOException {
		bW.write("<Style id=\"" + alias + "\">\n");
		bW.write("<IconStyle>\n");
		bW.write("<scale>1.1</scale>\n");
		bW.write("<Icon>\n");
		bW.write("<href>" + url + "\n");
		bW.write("</href>\n");
		bW.write("</Icon>\n");
		bW.write("<hotSpot x=\"" + x + "\" y=\"" + y + "\" xunits=\"pixels\" yunits=\"pixels\" />\n");
		bW.write("</IconStyle>\n");
		bW.write("<ListStyle>\n");
		bW.write("</ListStyle>\n");
		bW.write("</Style>\n");
	}
}
