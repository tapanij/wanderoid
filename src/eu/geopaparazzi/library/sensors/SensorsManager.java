/*
 * Geopaparazzi - Digital field mapping on Android based devices
 * Copyright (C) 2010  HydroloGIS (www.hydrologis.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package eu.geopaparazzi.library.sensors;

import static java.lang.Math.toDegrees;

import java.util.ArrayList;
import java.util.List;

import eu.geopaparazzi.library.util.debug.Debug;
import eu.geopaparazzi.library.util.debug.Logger;

import android.content.Context;
import android.content.res.Configuration;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;

/**
 * Singleton that takes care of sensor matters.
 * 
 * @author Andrea Antonello (www.hydrologis.com)
 */
public class SensorsManager implements SensorEventListener {

	private static SensorsManager sensorManager;

	private static WakeLock mWakeLock;

	private SensorManager sensorManagerInternal;
	private ConnectivityManager connectivityManager;

	private int accuracy;

	private double normalAzimuth = -1;
	private double normalPitch = -1;
	private double normalRoll = -1;
	private double pictureAzimuth = -1;
	private double picturePitch = -1;
	private double pictureRoll = -1;
	private double accelerometerZ = -1;
	private float[] mags;

	private boolean isReady;

	private float[] accels;
	private float[] orients;

	private final static int matrix_size = 16;
	private final float[] RM = new float[matrix_size];
	private final float[] outR = new float[matrix_size];
	private final float[] I = new float[matrix_size];
	private final float[] values = new float[3];

	private final Context context;

	private List<SensorsManagerListener> listeners = new ArrayList<SensorsManagerListener>();
	protected final Handler mHandler = new Handler();

	private SensorsManager(Context context) {
		this.context = context;

	}

	public synchronized static SensorsManager getInstance(Context context) {
		if (sensorManager == null) {
			if (Debug.D)
				Logger.d("sensorsmanager", "sensorManager null " );
			sensorManager = new SensorsManager(context);
			sensorManager.activateSensorManagers();
			sensorManager.startSensorListening();
		}
		return sensorManager;
	}

	/**
	 * Add a listener to gps.
	 * 
	 * @param listener
	 *            the listener to add.
	 */
	public void addListener(SensorsManagerListener listener) {
		if (Debug.D)
		Logger.d(this, "testing: addListener "+ listener );
		if (!listeners.contains(listener)) {
			listeners.add(listener);
		}
	}

	/**
	 * Remove a listener to gps.
	 * 
	 * @param listener
	 *            the listener to remove.
	 */
	public void removeListener(SensorsManagerListener listener) {
		listeners.remove(listener);
	}

	/**
	 * Get the location and sensor managers.
	 */
	public void activateSensorManagers() {
		if (Debug.D)
			Logger.d(this, "testing: activateSensorManagers " );
		sensorManagerInternal = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
		connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	}

	public boolean isInternetOn() {
		NetworkInfo info = connectivityManager.getActiveNetworkInfo();
		return (info != null && info.isConnected());
	}

	public int getAccuracy() {
		return accuracy;
	}

	public double getNormalAzimuth() {
		return normalAzimuth;
	}

	public double getPictureAzimuth() {
		return pictureAzimuth;
	}

	public double getNormalPitch() {
		return normalPitch;
	}

	public double getPicturePitch() {
		return picturePitch;
	}

	public double getNormalRoll() {
		return normalRoll;
	}

	public double getPictureRoll() {
		return pictureRoll;
	}
	
	public double getAccelerometerZ() {
		return accelerometerZ;
	}

	/**
	 * Stops listening to all the devices.
	 */
	public void stopSensorListening() {
		if (Debug.D)
			Logger.d(this, "testing: stopSensorListening " );
		if (sensorManagerInternal != null && sensorManager != null)
			if (Debug.D)
				Logger.d(this, "testing: stopSensorListening2 " );
			sensorManagerInternal.unregisterListener(sensorManager);
			sensorManagerInternal.unregisterListener(this);
			sensorManagerInternal.unregisterListener(sensorManager);
			sensorManagerInternal.unregisterListener(this);
	}

	/**
	 * Starts listening to all the devices.
	 */
	public void startSensorListening() {
		if (Debug.D)
			Logger.d(this, "testing: startSensorListening " );

		sensorManagerInternal.unregisterListener(sensorManager);
		sensorManagerInternal.registerListener(sensorManager, sensorManagerInternal.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
		sensorManagerInternal.registerListener(sensorManager, sensorManagerInternal.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), SensorManager.SENSOR_DELAY_NORMAL);
		sensorManagerInternal.registerListener(sensorManager, sensorManagerInternal.getDefaultSensor(Sensor.TYPE_ORIENTATION), SensorManager.SENSOR_DELAY_NORMAL);
	}

	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		int type = sensor.getType();
		if (type == SensorManager.SENSOR_ORIENTATION) {
			this.accuracy = accuracy;
		}
	}

	public void onSensorChanged(SensorEvent event) {

		/** Handing orientation sensors **/

		Sensor sensor = event.sensor;
		int type = sensor.getType();

		// Magnetic field and accelerometer forw getRotationMatrix @author Tapani Jamsa
		switch (type) {
		 case Sensor.TYPE_MAGNETIC_FIELD:
		 mags = event.values.clone();
		 isReady = true;
		 break;
		case Sensor.TYPE_ACCELEROMETER:
			accels = event.values.clone();
			accelerometerZ = accels[2];
			
//			if (Debug.D)
//				Logger.d(this, "accels[0] " + accels[0]);
//			if (Debug.D)
//				Logger.d(this, "accels[1] " + accels[1]);
//			if (Debug.D)
//				Logger.d(this, "accels[2] " + accels[2]);		
			
			break;
		case Sensor.TYPE_ORIENTATION:
			orients = event.values.clone();
			/**
			 * Handling compass Added different way of handing compass data @author
			 * Tapani J�ms�
			 **/
			float direction = event.values[0] * -1.0f;
			// Logger.d(sensorManager, "1 direction = " + direction);
			direction = normalizeDegree(direction);
			normalAzimuth = normalizeDegree(direction * -1.0f);
			 Logger.d(sensorManager, "1 pitch = " + event.values[1]);
			 Logger.d(sensorManager, "1 roll = " + event.values[2]);
			 Logger.d(sensorManager, "2 direction2 = " + direction);
//			Logger.d(sensorManager, "3 normalAzimuth = " + normalAzimuth);
			break;
		}

		if (mags != null && accels != null /*&& orients != null*/ && isReady) {
			isReady = false;

			SensorManager.getRotationMatrix(RM, I, accels, mags);
			SensorManager.remapCoordinateSystem(RM, SensorManager.AXIS_X, SensorManager.AXIS_Y, outR);
			SensorManager.getOrientation(outR, values);
//			double aBnormalAzimuth = toDegrees(values[0]);
			normalPitch = toDegrees(values[1]);
			normalRoll = toDegrees(values[2]);

			/* Changed original getContext() to context @author Tapani Jamsa int
			 orientation =
			 getContext().getResources().getConfiguration().orientation;*/
//			int orientation = context.getResources().getConfiguration().orientation;
//
//			switch (orientation) {
//			case Configuration.ORIENTATION_LANDSCAPE:
//				aBnormalAzimuth = -1 * (aBnormalAzimuth - 135);
//			case Configuration.ORIENTATION_PORTRAIT:
//			default:
//				break;
//			}
//			aBnormalAzimuth = aBnormalAzimuth > 0 ? aBnormalAzimuth : (360f + aBnormalAzimuth);
//			Logger.d(this, "aBnormalAzimuth = " + aBnormalAzimuth);
//
//			SensorManager.remapCoordinateSystem(RM, SensorManager.AXIS_X, SensorManager.AXIS_Z, outR);
//			SensorManager.getOrientation(outR, values);

			// pictureAzimuth = toDegrees(values[0]);
			picturePitch = toDegrees(values[1]);
			pictureRoll = toDegrees(values[2]);
			// pictureAzimuth = pictureAzimuth > 0 ? pictureAzimuth
			// : (360f + pictureAzimuth);
			//

			// Logger.d(sensorManager, "PAZIMUTH = " + pictureAzimuth);

			// for (SensorsManagerListener listener : listeners) {
			// listener.onSensorChanged(normalAzimuth, pictureAzimuth);
			// }
		}

	}

	private float normalizeDegree(float degree) {
		return (degree + 720) % 360;
	}
}
