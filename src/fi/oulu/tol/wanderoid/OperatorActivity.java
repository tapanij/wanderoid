/*
 * Wanderoid - An android application for tracking and monitoring the user of a mobile device
 * Copyright (C) 2013 Department of Information Processing Science, University of Oulu (http://www.oulu.fi/tol/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Latest source code available here https://bitbucket.org/samoojamies/wanderoid
 * 
 * This file includes source code from here: http://developer.android.com/reference/android/app/Service.html#LocalServiceSample
 */

package fi.oulu.tol.wanderoid;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import eu.geopaparazzi.library.gps.GpsManager;
import eu.geopaparazzi.library.sensors.SensorsManager;
import eu.geopaparazzi.library.util.debug.Debug;
import eu.geopaparazzi.library.util.debug.Logger;
import android.app.Activity;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.os.Bundle;
import android.os.Environment;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

/**
 * Takes a snapshot and then gets destroyed
 * Mixed and modified geopaparazzi's formUtilities.java: button.setOnClickListener and geopaparazziactivity.java: case (RETURNCODE_PICS): 
 * 
 * @author Tapani Jamsa (tapani.jamsa@gmail.com)
 */
public class OperatorActivity extends Activity implements SurfaceHolder.Callback {
	public static final int CAMERA_SLEEP_TIME = 1000; // 1000 default
	SurfaceHolder mSurfaceHolder;
	SurfaceView mSurfaceView;
	public Camera mCamera;
	boolean mPreviewRunning;
	private SensorsManager sensorsManager;
	private GpsManager gpsManager;

	/** intentservice stuff **/

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (Debug.D)
			Logger.d(this, "onCreate");

		/** camera preview stuff **/

		getWindow().setFormat(PixelFormat.TRANSLUCENT);
		setContentView(R.layout.operator);
		mSurfaceView = (SurfaceView) findViewById(R.id.surface_camera);
		mSurfaceHolder = mSurfaceView.getHolder();
		mSurfaceHolder.addCallback(this);
		// setType needed for older android versions
		mSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		
		sensorsManager = SensorsManager.getInstance(this);
		
		gpsManager = GpsManager.getInstance(this);
	}

	public void takePictureNoPreview() {
		if (Debug.D)
			Logger.d(this, "takePictureNoPreview");
		mCamera.takePicture(null, null, mPictureCallback);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		// Listening would continue forever without stopping it here
		sensorsManager.stopSensorListening();
		
		// Tell the user we stopped
		Toast.makeText(this, R.string.operator_service_stopped, Toast.LENGTH_SHORT).show();
	}

	@Override
	// Includes settings for the camera preview surface and starts the snapshotting process
	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
		if (Debug.D)
			Logger.d(this, "surfaceChanged");

		if (mPreviewRunning) {
			mCamera.stopPreview();
		}
		mCamera.setDisplayOrientation(90);
		Camera.Parameters p = mCamera.getParameters();
		// p.setPreviewSize(w, h);
		p.setPictureFormat(PixelFormat.JPEG);
		p.setPictureSize(640, 480);
		p.setJpegQuality(100); // a bitmap will get compressed when sending snapshot to the server
		p.setRotation(90);
		// Less blurry shots http://en.wikipedia.org/wiki/Shutter_speed
		p.setExposureCompensation(p.getMinExposureCompensation());
		p.setSceneMode(Parameters.SCENE_MODE_STEADYPHOTO);
		p.setFocusMode(Parameters.FOCUS_MODE_FIXED);
		// No thumbnail
//		FIXME this line parameter galaxy mini crash: p.setJpegThumbnailSize(1, 1);
		
		mCamera.setParameters(p);
		try {
			mCamera.setPreviewDisplay(arg0);
		} catch (IOException e) {
			e.printStackTrace();
		}
		mCamera.startPreview();
		mPreviewRunning = true;

		takePictureNoPreview();
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		if (Debug.D)
			Logger.d(this, "surfaceCreated");
		mCamera = Camera.open();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		if (Debug.D)
			Logger.d(this, "surfaceDestroyed");
		mCamera.stopPreview();
		mPreviewRunning = false;
		mCamera.release();
	}

	// Saves the picture and informs SensorService about it
	Camera.PictureCallback mPictureCallback = new Camera.PictureCallback() {
		@Override
		public void onPictureTaken(byte[] data, Camera camera) {
			if (Debug.D)
				Logger.d(this, "Image data is: " + data);
			OutputStream imageFileOS;
			try {
				imageFileOS = new FileOutputStream(String.format(Environment.getExternalStorageDirectory().getPath() + File.separator + "Wanderoid" + File.separator + "images" + File.separator
						+ "snapshot.jpg", System.currentTimeMillis()));
				imageFileOS.write(data);
				imageFileOS.flush();
				imageFileOS.close();

				Toast.makeText(OperatorActivity.this, "Image saved", Toast.LENGTH_LONG).show();
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			Intent i = new Intent("fi.oulu.tol.custom.intent.action.SNAPSHOT_DONE");
			sendBroadcast(i);

			finish();
		}
	};
}