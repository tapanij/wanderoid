/*
 * Wanderoid - An android application for tracking and monitoring the user of a mobile device
 * Copyright (C) 2013 Department of Information Processing Science, University of Oulu (http://www.oulu.fi/tol/)
 *
 * This file includes source code of Geopaparazzi - Digital field mapping on Android based devices
 * Copyright (C) 2010  HydroloGIS (www.hydrologis.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Latest source code available here https://bitbucket.org/samoojamies/wanderoid
 */

package fi.oulu.tol.wanderoid;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Date;
import com.ibm.mqtt.MqttClient;
import com.ibm.mqtt.MqttException;
import com.ibm.mqtt.MqttNotConnectedException;
import com.ibm.mqtt.MqttPersistenceException;
import com.ibm.mqtt.MqttSimpleCallback;

import eu.geopaparazzi.library.gps.GpsLocation;
import eu.geopaparazzi.library.gps.GpsManager;
import eu.geopaparazzi.library.gps.GpsManagerListener;
import eu.geopaparazzi.library.kml.KmzExport;
import eu.geopaparazzi.library.sensors.SensorsManager;
import eu.geopaparazzi.library.util.LibraryConstants;
import eu.geopaparazzi.library.util.debug.Debug;
import eu.geopaparazzi.library.util.debug.Logger;

import eu.hydrologis.geopaparazzi.util.Image;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.WifiLock;
import android.os.Binder;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.preference.PreferenceManager;
import android.provider.Settings.Secure;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

/**
 * Networking and sensor data
 * 
 * @author Tapani Jamsa (tapani.jamsa@gmail.com)
 */
public class SensorService extends Service implements GpsManagerListener, OnSharedPreferenceChangeListener {
	// Binder given to clients
	private final IBinder mBinder = new LocalBinder();
	private static DecimalFormat mFormatter = new DecimalFormat("0.00000"); //$NON-NLS-1$
	private static String nodataString;
	private static String timeString;
	private static String lonString;
	private static String latString;
	private static String altimString;
	private static String azimString;
	private static String pitchString;
	private static String rollString;
	private static String loggingString;
	private static String acquirefixString;
	private static String gpsonString;
	private String android_id;
	private GpsManager gpsManager;
	private NotificationManager mNM;
	// Unique Identification Number for the Notification.
	// We use it on Notification start, and to cancel it.
	private int NOTIFICATION = R.string.local_service_started;
	private MqttClient mClient;
	private SharedPreferences preferences;
	private int mSleepSeconds = 5;
	private boolean mRunning = true;
	private SensorsManager sensorsManager;
	private Thread mThread;
	private WakeLock mWakeLock;
	private WifiLock mWifiLock;
	private static final int mPeriodicalPublishInterval = 30; // in seconds

	// Initialize strings
	private void initVars() {
		timeString = getString(R.string.utctime);
		lonString = getString(R.string.lon);
		latString = getString(R.string.lat);
		altimString = getString(R.string.altim);
		azimString = getString(R.string.azimuth);
		nodataString = getString(R.string.nogps_data);
		loggingString = getString(R.string.text_logging);
		acquirefixString = getString(R.string.gps_searching_fix);
		gpsonString = getString(R.string.text_gpson);
		pitchString = getString(R.string.pitch);
		rollString = getString(R.string.roll);
	}

	// All messages will get handled here
	final Handler mHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			if (Debug.D)
				Logger.d(this, "New message arrived: ");
			if (Debug.D && msg.getData().getString("topic") != null && msg.getData().getString("message") != null) {
				Logger.d(this, msg.getData().getString("topic") + "\n" + msg.getData().getString("message"));
			} else {
				Logger.d(this, "The message is null!");
			}

			if (msg.getData().getString("message").equals("snapshot")) {
				if (Debug.D)
					Logger.d(this, "Got a snapshot command from the server. Start snapshotting.");

				Intent activity = new Intent(getBaseContext(), OperatorActivity.class);
				activity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_USER_ACTION | Intent.FLAG_FROM_BACKGROUND);
				getBaseContext().startActivity(activity);
			} else if (msg.getData().getString("message").equals("periodical")) {
				if (Debug.D)
					Logger.d(this, "Got a periodiocal message");
				try {
					periodicalPublish();
				} catch (MqttNotConnectedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (MqttPersistenceException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (MqttException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	};

	/**
	 * This starts the connection process. tryToConnect() and connect() are sub
	 * methods
	 **/
	private void createConnectionThread() {
		// Connect to server
		mRunning = true;

		mThread = new Thread(new Runnable() {

			public void run() {
				Log.d("thread", "Trying to connect");
				tryToConnect();
			}
		});
		mThread.start();
	}

	private void tryToConnect() {
		do {// pause and try again;
			if (Debug.D)
				Logger.d(this, "sleeping for " + mSleepSeconds + " seconds before trying to reconnect");
			try {
				Thread.sleep(mSleepSeconds * 1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} while (mRunning && !connect());
		subscribe();
	}

	private boolean connect() {
		String server = null;
		try {
			server = preferences.getString("ip_address", "0");
		} catch (Exception e) {
			Log.v("MyApp", e.toString());
		}
		try {
			if (Debug.D)
				Logger.d(this, "Connecting to: " + server);
			mClient = (MqttClient) MqttClient.createMqttClient("tcp://" + server, null);
			mClient.registerSimpleHandler(new MessageHandler());
			mClient.connect("HM" + android_id, true, (short) 240);
			showConnection(true);
			if (Debug.D)
				Logger.d(this, "connected");

			// This makes sure that no two threads trying to connect same time
			// to the server
			mRunning = false;

			// Publish data to the server
			periodicalPublish();

			return true;
		} catch (MqttException e) {
			e.printStackTrace();
			return false;
		}
	}

	// Publishes data periodically to the server
	private void periodicalPublish() throws MqttNotConnectedException, MqttPersistenceException, IllegalArgumentException, MqttException {
		if (!mRunning) {
			try {
				// Create a new KML without an image
				KmzExport export = new KmzExport(null);
				export.export(getBaseContext(), getSensorData());

				File kmlFile = new File(Environment.getExternalStorageDirectory().getPath() + File.separator + "Wanderoid" + File.separator + "kml" + File.separator + "kml.kml");
				if (!kmlFile.exists()) {
					return;
				}

				// create FileInputStream object
				FileInputStream fin = new FileInputStream(kmlFile);

				/*
				 * Create byte array large enough to hold the content of the
				 * file. Use File.length to determine size of the file in bytes.
				 */

				byte fileContent[] = new byte[(int) kmlFile.length()];

				/*
				 * To read content of the file in byte array, use int
				 * read(byte[] byteArray) method of java FileInputStream class.
				 */
				fin.read(fileContent);

				byte[] base64 = Base64.encode(fileContent, Base64.DEFAULT);

				mClient.publish("kmlfile", base64, 1, false);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// Start periodiocal data sending to the server
			Bundle b = new Bundle();
			b.putString("message", "periodical");
			Message msg = mHandler.obtainMessage();
			msg.setData(b);
			mHandler.sendMessageDelayed(msg, mPeriodicalPublishInterval * 1000);
		}
	}

	private Image getSensorData() {
		if (Debug.D)
			Logger.d(this, "getImageData");
		GpsLocation loc = gpsManager.getLocation();

		double lon = 0;
		double lat = 0;
		double azim = 0;
		double altim = 0;
		long id = 1;

		if (loc == null || !gpsManager.isEnabled()) {
			if (Debug.D)
				Logger.d(this, "Location from gps is null!");
		} else {
			lon = loc.getLongitude();
			lat = loc.getLatitude();
			azim = sensorsManager.getNormalAzimuth();
			altim = loc.getAltitude();
			if (Debug.D)
				Logger.d(this, "Retrived gps data");
		}
		double pitch = sensorsManager.getNormalPitch();
		double roll = sensorsManager.getNormalRoll();
		double accelZ = sensorsManager.getAccelerometerZ();

		Date currentDate = new Date();
		String currentDatestring = LibraryConstants.TIMESTAMPFORMATTER.format(currentDate);

		Image image = new Image(id, lon, lat, azim, pitch, roll, accelZ, altim, false, currentDatestring);
		return image;
	}

	// Handles incoming MQTT messages
	private class MessageHandler implements MqttSimpleCallback {
		public void publishArrived(String _topic, byte[] payload, int qos, boolean retained) throws Exception {
			String _message = new String(payload);
			Bundle b = new Bundle();
			b.putString("topic", _topic);
			b.putString("message", _message);
			Message msg = mHandler.obtainMessage();
			msg.setData(b);
			mHandler.sendMessage(msg);
		}

		public void connectionLost() throws Exception {
			showConnection(false);
			mClient = null;
			Log.v("SensorService", "connection dropped");
			// This is needed so we automatically reconnect to the server when
			// connection drops @author Tapani Jamsa
			createConnectionThread();
		}
	}

	// Publishes data to the server
	public void publish() throws IOException {
		// Check if we are connected to the server
		if (!mRunning) {
			try {
				/** KML publish**/
				KmzExport export = new KmzExport(null);
				export.export(getBaseContext(), getSensorData());
				
				File kmlFile = new File(Environment.getExternalStorageDirectory().getPath() + File.separator + "Wanderoid" + File.separator + "kml" + File.separator + "kml.kml");
				if (!kmlFile.exists()) {
					return;
				}

				// create FileInputStream object
				FileInputStream fin = new FileInputStream(kmlFile);

				/*
				 * Create byte array large enough to hold the content of the
				 * file. Use File.length to determine size of the file in bytes.
				 */

				byte fileContent[] = new byte[(int) kmlFile.length()];

				/*
				 * To read content of the file in byte array, use int
				 * read(byte[] byteArray) method of java FileInputStream class.
				 */
				fin.read(fileContent);

				byte[] base64 = Base64.encode(fileContent, Base64.DEFAULT);

				mClient.publish("kmlfile", base64, 1, false);
				
				
				/** SNAPSHOT publish **/

				Bitmap bm = BitmapFactory.decodeFile(Environment.getExternalStorageDirectory().getPath() + File.separator + "Wanderoid" + File.separator + "images" + File.separator + "snapshot.jpg");
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				bm.compress(Bitmap.CompressFormat.JPEG, 40, baos); // bm is the
																	// bitmap
																	// object
				byte[] b = baos.toByteArray();

				base64 = Base64.encode(b, Base64.DEFAULT);

				mClient.publish("picture", base64, 1, false);
			} catch (MqttNotConnectedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (MqttPersistenceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (MqttException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	// Subscribes to MQTT messages
	private void subscribe() {
		try {
			String topics[] = { "snapshot" };
			int qos[] = { 1 };
			if (mClient != null) {
				mClient.subscribe(topics, qos);
			}
		} catch (MqttException e) {
			if (Debug.D)
				Logger.d(this, "Subscribe Failed!");
		}
		if (Debug.D)
			Logger.d(this, "Subscribe Success!");
	}

	// private void unSubscribe() {
	// try {
	// String topics[] = { "#" };
	// client.unsubscribe(topics);
	// } catch (MqttException e) {
	// // Toast.makeText(this, "Unsubscribe Failed!",
	// Toast.LENGTH_SHORT).show();
	// }
	// // Toast.makeText(this, "Unsubscribe Success!",
	// Toast.LENGTH_SHORT).show();
	// }

	/** Networking ends **/

	/**
	 * Show a connection notification about server connection.
	 */
	private void showConnection(Boolean connected) {
		CharSequence text;

		Notification notification;
		if (connected) {
			text = getText(R.string.server_connected);
			notification = new Notification(R.drawable.rating_good, text, System.currentTimeMillis());
		} else {
			text = getText(R.string.server_disconnected);
			notification = new Notification(R.drawable.alerts_and_states_warning, text, System.currentTimeMillis());
		}

		// The PendingIntent to launch our activity if the user selects this
		// notification
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0, new Intent(this, Wanderoid.class), 0);

		// Set the info for the views that show in the notification panel.
		notification.setLatestEventInfo(this, getText(R.string.local_service_label), text, contentIntent);

		// Send the notification.
		mNM.notify(NOTIFICATION, notification);
	}

	/**
	 * Show a notification while this service is running.
	 */
	private void showNotification() {
		// In this sample, we'll use the same text for the ticker and the
		// expanded notification
		CharSequence text = getText(R.string.local_service_started);

		// Set the icon, scrolling text and timestamp
		Notification notification = new Notification(R.drawable.ic_launcher, text, System.currentTimeMillis());

		// The PendingIntent to launch our activity if the user selects this
		// notification
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0, new Intent(this, Wanderoid.class), 0);

		// Set the info for the views that show in the notification panel.
		notification.setLatestEventInfo(this, getText(R.string.local_service_label), text, contentIntent);

		// Send the notification.
		mNM.notify(NOTIFICATION, notification);
	}

	// Creates sensor info for KML files
	public String createSensorInfo() {

		double azimuth = sensorsManager.getNormalAzimuth();
		double pitch = sensorsManager.getNormalPitch();
		double roll = sensorsManager.getNormalRoll();
		GpsLocation loc = gpsManager.getLocation();

		StringBuilder sb = new StringBuilder();
		if (loc == null || !gpsManager.isEnabled()) {
			Logger.d("COMPASSVIEW", "Location from gps is null!");
			sb.append(nodataString);
			sb.append("\n");
			if (gpsManager.isEnabled()) {
				if (!gpsManager.hasValidData()) {
					sb.append(acquirefixString);
				} else {
					sb.append(gpsonString);
					sb.append(": ").append(gpsManager.isEnabled()); //$NON-NLS-1$
				}
			}
			sb.append("\n");
		} else {
			sb.append(timeString);
			sb.append(" ").append(loc.getTimeString()); //$NON-NLS-1$
			sb.append("\n");
			sb.append("\n");
			sb.append(latString);
			sb.append(" ").append(mFormatter.format(loc.getLatitude())); //$NON-NLS-1$
			sb.append("\n");
			sb.append(lonString);
			sb.append(" ").append(mFormatter.format(loc.getLongitude())); //$NON-NLS-1$
			sb.append("\n");
			sb.append(altimString);
			sb.append(" ").append((int) loc.getAltitude()); //$NON-NLS-1$
			sb.append("\n");
			sb.append("\n");
			// Seems like a phone must be in potrait mode or the azimuth shows a
			// wrong value @author Tapani Jamsa
			sb.append(azimString);
			sb.append(" ").append((int) azimuth); //$NON-NLS-1$
			sb.append("\n");
			sb.append("\n");
			sb.append(pitchString);
			sb.append(" ").append((int) pitch); //$NON-NLS-1$
			sb.append("\n");
			sb.append(rollString);
			sb.append(" ").append((int) roll); //$NON-NLS-1$
			sb.append("\n");
			sb.append("\n");
			sb.append(loggingString);
			sb.append(": ").append(gpsManager.isLogging()); //$NON-NLS-1$
		}
		return sb.toString();
	}

	/**
	 * Class used for the client Binder. Because we know this service always
	 * runs in the same process as its clients, we don't need to deal with IPC.
	 */
	public class LocalBinder extends Binder {
		SensorService getService() {
			// Return this instance of LocalService so clients can call public
			// methods
			return SensorService.this;
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}

	@Override
	public void onLocationChanged(GpsLocation loc) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStatusChanged(boolean hasFix) {
		// TODO Auto-generated method stub

	}

	public int onStartCommand(Intent intent, int flags, int startId) {
		if (Debug.D)
			Logger.d(this, "SensorService onStartCommand");

		// TODO Not sure if this helps anything (to keep service running when a screen is off). Try without it
		// Keep service foreground
		Notification note = new Notification(0, null, System.currentTimeMillis());
		note.flags |= Notification.FLAG_NO_CLEAR;
		startForeground(42, note);

		// correct place for this?
		initVars();

		GpsManager.getInstance(this).addListener(this);
		gpsManager = GpsManager.getInstance(this);

		sensorsManager = SensorsManager.getInstance(this);
		sensorsManager.startSensorListening();

		return START_STICKY;
	}

	@Override
	public void onCreate() {
		// Creation of folders
		File folder = new File(Environment.getExternalStorageDirectory() + File.separator + "Wanderoid" + File.separator + "images");
		// Create output folder
		if (!folder.exists()) {
			if (Debug.D)
				Logger.d(this, "image folder doesn't exist");
			folder.mkdirs();
		}

		// Create the KML folder
		folder = new File(Environment.getExternalStorageDirectory() + File.separator + "Wanderoid" + File.separator + "kml");
		if (!folder.exists()) {
			if (Debug.D)
				Logger.d(this, "kml folder doesn't exist");
			folder.mkdirs();
		}

		PowerManager pm = (PowerManager) getApplicationContext().getSystemService(Context.POWER_SERVICE);

		mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "SensorService");
		mWakeLock.acquire();

		// TODO not sure if this helps anything
		WifiManager wm = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
		mWifiLock = wm.createWifiLock(3, "SensorService");
		mWifiLock.acquire();

		preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

		preferences.registerOnSharedPreferenceChangeListener(this);

		// Display a notification about starting the service. We put an icon in the status bar.
		showNotification();

		android_id = Secure.getString(this.getContentResolver(), Secure.ANDROID_ID);

		createConnectionThread();

		// Original mqtt messenger thread code:
		// new Thread() {
		// int flag;
		// public void run() {
		// if(connect())
		// flag=1;
		// else
		// flag=0;
		// handlerConnect.sendMessage(Message.obtain(handlerConnect,flag)); }
		// }.start(); //Connect in a new thread!

		registerReceiver(mBroadcastReceiver, new IntentFilter("fi.oulu.tol.custom.intent.action.SNAPSHOT_DONE"));
		
		showConnection(false);
	}

	public void onDestroy() {
		if (Debug.D)
			Logger.d(this, "onDestroy ");

		super.onDestroy();

		mWakeLock.release();

		try {
			if (Debug.D)
				Logger.d(this, "Disconnect client");
			if (mClient != null) {
				mClient.disconnect();
				mClient.terminate();
			}
		} catch (MqttPersistenceException e) {
			e.printStackTrace();
		}
		if (preferences != null) {
			preferences.unregisterOnSharedPreferenceChangeListener(this);
			preferences = null;
		}
		if (sensorsManager != null) {
			sensorsManager.stopSensorListening();
			sensorsManager = null;
		}
		if (gpsManager != null) {
			gpsManager.removeListener(this);
			gpsManager = null;
		}

		mHandler.removeMessages(0);

		// Stop the thread
		mRunning = false;

		GpsManager.getInstance(this).removeListener(this);
		if (Debug.D)
			Logger.d(this, "Stopping Service");

		// Cancel the persistent notification.
		mNM.cancel(NOTIFICATION);

		unregisterReceiver(mBroadcastReceiver);

		mWifiLock.release();

		// Tell the user we stopped.
		Toast.makeText(this, R.string.local_service_stopped, Toast.LENGTH_SHORT).show();
	}

	@Override
	// Settings have been changed in the preference settings activity
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String preference) {
		// Validate user input. Ip address changed?
		if (preference.equals("ip_address")) {
			try {

				if (Debug.D)
					Logger.d(this, "Disconnect client");
				if (mClient != null) {
					mClient.disconnect();
					mClient.terminate();
				}
				mClient = null;
				showConnection(false);

				// TODO not sure if thread interrupt is necessary.
				// http://developer.android.com/reference/java/lang/Thread.html#interrupt()
				mThread.interrupt();

				createConnectionThread();
			} catch (MqttPersistenceException e) {
				e.printStackTrace();
			}
		}

	}

	// Receives a message from OperatorActivity about finished snapshotting. Publish the snapshot to the server
	private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
		public static final String CUSTOM_INTENT = "fi.oulu.tol.custom.intent.action.SNAPSHOT_DONE";

		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals(CUSTOM_INTENT)) {
				if (Debug.D)
					Logger.d(this, "GOT THE INTENT");
				try {
					publish();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	};
}
