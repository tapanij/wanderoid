/*
 * Wanderoid - An android application for tracking and monitoring the user of a mobile device
 * Copyright (C) 2013 Department of Information Processing Science, University of Oulu (http://www.oulu.fi/tol/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Latest source code available here https://bitbucket.org/samoojamies/wanderoid
 */
package fi.oulu.tol.wanderoid;

import eu.geopaparazzi.library.gps.GpsManager;
import fi.oulu.tol.wanderoid.R;
import android.os.Bundle;
import android.preference.PreferenceActivity;

/**
 * Prefence settings activity of Wanderoid.
 * 
 * @author Tapani Jamsa (tapani.jamsa@gmail.com)
 */
public class PrefSettingsActivity extends PreferenceActivity {

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		addPreferencesFromResource(R.xml.prefs);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		
		//Restart gpsManager
		GpsManager gpsManager = GpsManager.getInstance(getApplicationContext());
		gpsManager.dispose(getApplicationContext());
		gpsManager.startListening();
	}
}
