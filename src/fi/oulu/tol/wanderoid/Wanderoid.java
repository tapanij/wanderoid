/*
 * Wanderoid - An android application for tracking and monitoring the user of a mobile device
 * Copyright (C) 2013 Department of Information Processing Science, University of Oulu (http://www.oulu.fi/tol/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Latest source code available here https://bitbucket.org/samoojamies/wanderoid
 */
package fi.oulu.tol.wanderoid;

import eu.geopaparazzi.library.util.debug.Debug;
import eu.geopaparazzi.library.util.debug.Logger;
import fi.oulu.tol.wanderoid.SensorService.LocalBinder;

import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

/**
 * Debug interface of Wanderoid.
 * 
 * @author Tapani Jamsa (tapani.jamsa@gmail.com)
 */
public class Wanderoid extends Activity {
	/** Binder stuff begins **/

	SensorService mService;
	boolean mBound = false;

	/** Defines callbacks for service binding, passed to bindService() */
	private ServiceConnection mConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			// We've bound to LocalService, cast the IBinder and get
			// LocalService instance
			LocalBinder binder = (LocalBinder) service;
			mService = binder.getService();
			mBound = true;

		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			mBound = false;

		}
	};
	
	public void getSensorData(View view) {
		if (mBound) {
			// if (Debug.D)
			// Logger.d(this, "GPS data: " + mService.createGpsInfo());

			TextView textView = (TextView) findViewById(R.id.textView1);
			if (textView != null) {
				textView.setText(mService.createSensorInfo());
			}
		} else {
			if (Debug.D)
				Logger.d(this, "Service not bound");
		}

	}
	
	private boolean isServiceRunning() {
		ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
			if (SensorService.class.getName().equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}

	/** Binder stuff ends **/

	@Override
	protected void onResume() {
		super.onResume();
		if (Debug.D)
			Logger.d(this, "onResume");

		// We need to bind the server to SensorService after we come back from the settings activity
		LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !mBound) {
			Intent intent = new Intent(this, SensorService.class);
			bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
		if (Debug.D)
			Logger.d(this, "onStart");

		LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

		// Start up SensorService
		if (!isServiceRunning()) { // not sure if this is necessary
			if (Debug.D)
				Logger.d(this, "Service not running. Start service");
			if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
				buildAlertMessageNoGps();
			} else {
				// Bind to LocalService
				Intent intent = new Intent(this, SensorService.class);
				bindService(intent, mConnection, Context.BIND_AUTO_CREATE);

				startService(new Intent(getBaseContext(), SensorService.class));
			}
		} else {
			if (Debug.D)
				Logger.d(this, "Service already running");
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (Debug.D)
			Logger.d(this, "onPause");

		/* Leaving this activity onPause or onStop will cause
		 "showStatusIcon on inactive InputConnection" error when snapshotting.
		 So always call finish() when not using this user interface */
		finish();
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (Debug.D)
			Logger.d(this, "onStop");
		// Unbind from the service
		if (mBound) {
			unbindService(mConnection);
			mBound = false;
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (Debug.D)
			Logger.d(this, "onDestroy");
	}

	private void buildAlertMessageNoGps() {
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(getString(R.string.gps_dialog)).setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
				startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
				finish();
			}
		}).setNegativeButton("No", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
				dialog.cancel();
				finish();
			}
		});
		final AlertDialog alert = builder.create();
		alert.show();
	}

	public void takeShot(View view) {
        Intent activity = new Intent(getBaseContext(), OperatorActivity.class);
        activity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        getBaseContext().startActivity(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_wanderoid);
	}

	/* Creates the menu items */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(Menu.NONE, Menu.FIRST, Menu.NONE, "Settings").setIcon(android.R.drawable.ic_menu_preferences);
		return true;
	}

	/* Handles item selections */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent = new Intent();
		switch (item.getItemId()) {

		case Menu.FIRST:
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.setClass(getBaseContext(), PrefSettingsActivity.class);
			startActivity(intent);
			return true;
		}
		return false;
	}

	public void shutDown(View view) {
		if (Debug.D)
			Logger.d(this, "Shutting down app");

		if (mService != null) {
			mService.stopSelf();
		}

		finish();
	}
}
